# Whole-genome sequencing of non-Kp isolates

Basic analyses were performed using same modules and functions in 40EN project. These include:
- Quality check 
- Reads trimming
- De novo assembly and assembly quality control
- Kleborate

## "raw_040124" batch

Raw reads quality
```bash
cd raw_040124
source /data/SiNguyen/40EN/modules/quality_control.sh
wgs_qc $PWD fastqc_130124 multiqc_130124
```

Assembly
```bash
source /data/SiNguyen/40EN/modules/assembly.sh
assem_unicycler_series ./raw_040124/ unicycler_raw040124/ 8

(
    cd unicycler_raw040124/
    for f in */; do newfile=${f%%/*}.assembly.fasta; mv ${f}assembly.fasta ./$newfile; done
    for f in */; do newfile=${f%%/*}.assembly.gfa; mv ${f}assembly.gfa ./$newfile; done
    for f in */; do newfile=${f%%/*}.unicycler.log; mv ${f}unicycler.log ./$newfile; done 

    assembly_qc $PWD checkm Klebsiella 8
)
```

Kleborate
```bash
conda activate py38
kleborate-runner.py --assemblies ./unicycler_raw040124/*.assembly.fasta \
    --all -o kleborate_output.txt
```

Gene annotation
```bash
data_dir="./unicycler_raw040124"

conda activate bakta-latest # Bakta v1.8.1

BAKTA_DB="/data/SiNguyen/bakta_db_5.0_2023-02-20/db"

mkdir -p bakta_140124

for contig_file in ${data_dir}/*.fasta; do
    filename=${contig_file##*/};
    sampleID=${filename%%.*};
    # echo $sampleID;
    # mkdir -p "bakta_140124/"${sampleID}"/"; #output directory

    bakta --db ${BAKTA_DB} --genus Klebsiella --gram '-' \
        --prefix ${sampleID} --output "bakta_140124/"${sampleID} \
        --threads 8 ${contig_file};
done

### Check if all runs have been successful.
( cd bakta_140124
count=0
for f in */*.log; do
    i=$(tail $f | grep -c "write JSON");
    count=$(bc -l <<< $count+$i);
done
echo $count ) # should return 9
```