Sequence(s):
Length: 5323421
Count: 97
GC: 54.6
N50: 325561
N ratio: 0.0
coding density: 88.6

Annotation:
tRNAs: 79
tmRNAs: 1
rRNAs: 4
ncRNAs: 69
ncRNA regions: 45
CRISPR arrays: 1
CDSs: 4933
pseudogenes: 5
hypotheticals: 55
signal peptides: 494
sORFs: 15
gaps: 0
oriCs: 3
oriVs: 0
oriTs: 0

Bakta:
Software: v1.8.1
Database: v5.0, full
DOI: 10.1099/mgen.0.000685
URL: github.com/oschwengers/bakta
