Sequence(s):
Length: 5376670
Count: 142
GC: 57.7
N50: 528961
N ratio: 0.0
coding density: 88.5

Annotation:
tRNAs: 81
tmRNAs: 1
rRNAs: 5
ncRNAs: 72
ncRNA regions: 53
CRISPR arrays: 0
CDSs: 5029
pseudogenes: 7
hypotheticals: 34
signal peptides: 504
sORFs: 17
gaps: 0
oriCs: 5
oriVs: 3
oriTs: 2

Bakta:
Software: v1.8.1
Database: v5.0, full
DOI: 10.1099/mgen.0.000685
URL: github.com/oschwengers/bakta
